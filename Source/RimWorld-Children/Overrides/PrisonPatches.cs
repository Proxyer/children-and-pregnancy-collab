﻿namespace RimWorldChildren {
    using HarmonyLib;
    using RimWorld;
    using Verse;

    /// <summary>
    /// All patches pertaining to prisoners, imprisonment, etc
    /// </summary>
    public class PrisonPatches {
        [HarmonyPatch(typeof(PrisonBreakUtility), "CanParticipateInPrisonBreak")]
        /// <summary>
        /// Patch PrisonBreakUtility to ensure that certain pawns are too young to participate or initiate 
        /// a prison break/
        /// </summary>
        public class PrisonBreakUtility_CanParticipateInPrisonBreak_Patch {
            [HarmonyPostfix]
            internal static void CanParticipateInPrisonBreak_Postfix(Pawn pawn, ref bool __result) {
                bool isTooYoung = pawn.TryGetComp<LifecycleComp>() != null && pawn.TryGetComp<LifecycleComp>().CurrentLifestage.lifestageType <= LifestageType.Toddler;
                __result = __result && !isTooYoung;
            }
        }
    }
}
