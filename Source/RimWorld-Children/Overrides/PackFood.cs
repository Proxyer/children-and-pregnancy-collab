﻿namespace RimWorldChildren {
    using HarmonyLib;
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;
    using Verse.AI;

    // Prevents babies from taking food into inventory
    [HarmonyPatch(typeof(JobGiver_PackFood), "TryGiveJob")]
    public static class PackFood_Override {
        [HarmonyPostfix]
        internal static void TryGiveJob_Postfix(ref Pawn pawn, ref Job __result) {
            if (ChildrenUtility.GetLifestageType(pawn) < LifestageType.Child && ChildrenUtility.RaceUsesChildren(pawn)) {
                __result = null;
            }
        }
    }
}