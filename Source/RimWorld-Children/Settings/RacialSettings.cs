﻿namespace RimWorldChildren {
    using Verse;

    /// <summary>
    /// Race specific user configuration model.
    /// NOTE: Developers should only add settings here if absolutely necessary. The majority of race specifics
    /// configs belong in the racial def. Only those user levers that are necessary should be added here.
    /// </summary>
    public class RacialSettings : IExposable {
        // If false, Prevents female pawns of this race from becoming pregnant from any source
        public bool pregnancyEnabled;

        // If false, prevents the lifeCycle component from operating on this pawn
        public bool lifeCycleEnabled;

        public void ExposeData() {
            Scribe_Values.Look(ref pregnancyEnabled, "pregnancyEnabled");
            Scribe_Values.Look(ref lifeCycleEnabled, "lifeCycleEnabled");
        }
    }
}
