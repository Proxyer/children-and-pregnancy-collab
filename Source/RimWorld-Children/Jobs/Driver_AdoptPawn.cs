﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using RimWorld;
    using RimWorldChildren;
    using RimWorldChildren.API;
    using Verse;
    using Verse.AI;

    /// <summary>
    /// This job will handle the adoption of any type of child pawn from another faction.
    /// </summary>
    public class Driver_AdoptPawn : JobDriver {
        private Pawn Child {
            get {
                return (Pawn)TargetThingA;
            }
        }

        public override bool TryMakePreToilReservations(bool errorOnFailed) {
            return this.pawn.Reserve(Child, this.job, 1, -1, null, errorOnFailed);
        }

        protected override IEnumerable<Toil> MakeNewToils() {
            this.FailOnDespawnedOrNull(TargetIndex.A);
            yield return Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.Touch).FailOn(() => !ChildrenUtility.CanBeAdopted(Child));
            Toil adopt = new Toil();
            adopt.initAction = () => {
                if (Child.Faction != null && Child.Faction != Faction.OfPlayer && (!Child.Faction.def.hidden && !Child.Faction.HostileTo(Faction.OfPlayer)) && !Child.IsPrisonerOfColony && Child.Map.mapPawns.PawnsInFaction(Child.Faction).Count > 1) {
                    Child.guest.CapturedBy(Faction.OfPlayer, pawn);
                }

                if (Child.guest != null) {
                    Child.guest.SetGuestStatus(null, false);
                }

                if (Child.Faction != pawn.Faction) {
                    Child.SetFaction(pawn.Faction, pawn);
                }

                TaleRecorder.RecordTale(TaleDefOf.Recruited, pawn, Child);
                if (Child.needs.mood != null) {
                    Child.needs.mood.thoughts.memories.TryGainMemory(CnpThoughtDefOf.AdoptedMe, pawn);
                }

                Find.LetterStack.ReceiveLetter("NoteAdoption".Translate(), TranslatorFormattedStringExtensions.Translate("MessageAdopted", Child.LabelIndefinite()), LetterDefOf.PositiveEvent, Child);

            };
            yield return adopt;
        }
    }
}
