﻿namespace RimWorldChildren {
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;

    public class InteractionWorker_HugFriend : InteractionWorker {
        public override float RandomSelectionWeight (Pawn initiator, Pawn recipient) {
            if (initiator.relations.OpinionOf(recipient) >= 50 && initiator.needs.mood.CurLevel >= 0.9f && ChildrenUtility.GetLifestageType(initiator) <= LifestageType.Child && ChildrenUtility.GetLifestageType(initiator) > LifestageType.Baby) {
                return 0.2f;
            }
            return 0;
        }
    }
}