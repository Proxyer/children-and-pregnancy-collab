﻿namespace RimWorldChildren {
    using RimWorld;
    using Verse;

    /// <summary>
    /// Static accessors for child hediff defs
    /// </summary>
    [DefOf]
    public static class ChildHediffDefOf {
        public static HediffDef UnhappyBaby;
        public static HediffDef LifestageCapModInjections;

        static ChildHediffDefOf() {
            DefOfHelper.EnsureInitializedInCtor(typeof(ChildHediffDefOf));
        }

    }
}
